---parse.config
-- @author Chris Byerley
local config =
{
  version = "0.2.7",

  cloud_address = "https://api.parse.com/1",

  application_id = nil,
  rest_api_key = nil,

  debug_enabled = false,
  debug_verbose = false,

  install_id = nil,
  master_key = nil,

  session_token = nil,
  timeout = 30,

  --== ?
  debug_meta = false,

  --==Constants
  Type =
  {
    Pointer   = "Pointer",
    Relation  = "Relation",
    File      = "File",
    Bytes     = "Bytes",
    Date      = "Date",
    GeoPoint  = "GeoPoint",
    User      = "_User",
    Role      = "_Role"
  },

  Op =
  {
    Increment       = "Increment",
    AddRelation     = "AddRelation",
    RemoveRelation  = "RemoveRelation",
    AddUnique       = "AddUnique",
    Add             = "Add",
    Remove          = "Remove"
  }
}

--== Set a config flag
function config:set( key, val, overwrite )
  local overwrite = overwrite or false

  if self[ key ] and overwrite == false then
    print('Key exists. Use overwrite flag')
    return
  end

  if val == nil then
    print('Value parameter missing.')
    return
  end

  self[ key ] = val
end

--== Get a config flag
function config:get( key )
  if self[ key ] then
    return self[ key ]
  end
  return nil
end

--== Session token
function config:sessionToken( session_token )
  if session_token == nil then
    return self.session_token
  end
  self.session_token = session_token
end

function config:clearSession()
  if self.session_token then
    self.session_token = nil
  end
end

--== Server Address
function config:cloudAddress( cloud_addr )
  if cloud_addr == nil then
    return self.cloud_address
  end
  self.cloud_address = cloud_addr
end

--== Application ID
function config:applicationId( app_id )
  if app_id == nil then
    return self.application_id
  end
  self.application_id = app_id
end

--== REST API key
function config:restApiKey( str_key )
  if str_key == nil then
    return self.rest_api_key
  end
  self.rest_api_key = str_key
end

--== Master Key
function config:masterKey( master_key )
  if master_key == nil then
    return self.master_key
  end
  self.master_key = master_key or nil
end

--== Request Timeout
function config:timeout( seconds )
  if seconds == nil then
    return self.timeout
  end
  self.timeout = seconds or 30
end

--== Installation ID
function config:installationId( install_id )
  if install_id == nil then
    return self.install_id
  end
  self.install_id = install_id or nil
end

--== Debug Mode
function config:debugEnabled( state )
  local state = state or nil
  if state == nil then
    return self.debug_enabled
  end
  self.debug_enabled = state
end

--== Debug verbosity
function config:debugVerbose( state )
  local state = state or nil
  if state == nil then
    return self.debug_verbose
  end
  self.debug_verbose = state
end

--== Client key - USE MANUAL HEADER
-- function config:clientKey( str_key )
--   if str_key == nil then
--     return self.client_key
--   end
--   self.client_key = str_key
-- end

--== Account Email - USE MANUAL HEADER
-- function config:accountEmail( email )
--   if email == nil then
--     return self.account_email
--   end
--   self.account_email = email
-- end

--== Account Pass - USE MANUAL HEADER
-- function config:accountPasswd( pass )
--   if pass == nil then
--     return self.account_passwd
--   end
--   self.account_passwd = email
-- end

return config

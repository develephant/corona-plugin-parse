---parse.debug
-- @author Chris Byerley
local config = require('plugin.parse.config')
local tools = require('plugin.parse.tools')

local debug = {}

function debug:process( response_data, response_meta, request_data, force_debug, call_count )
  local force_debug = force_debug or false
  
  local verbose = config:debugVerbose()
  
  if force_debug or config:debugEnabled() then
    print("********************************************************************")
    print("** Parse RESPONSE @ ".. tools.timestampToDate() .. " ["..call_count.."]" )
    print("********************************************************************")
  
    --print("===> RESPONSE DATA")

    if response_data then
      tools.trace( response_data, '> ' )
      print("********************************************************************")
    end
  
    if verbose then
      print("** RESPONSE META")
      if response_meta then
        tools.trace( response_meta )
      end
      
      print("====================================================================")
    end
    
    if verbose then
      print("** REQUEST DATA")
      if request_data then
        tools.trace( request_data )
      end

      print("====================================================================")
    end
  end
end

return debug

---parse.response
-- @author Chris Byerley
local json2table = require('plugin.parse.tools').json2table
local debug = require('plugin.parse.debug')

local response =
{
  call_count = 0
}

function response.parse( response_event, request_data, force_debug )

  response.call_count = response.call_count + 1

  local force_debug = force_debug or false

  local ok, res = false, "An unknown error occurred"

  local suppress = { 'name', 'phase', 'requestId', 'response' }
  suppress = table.concat( suppress, "," )

  local meta = {}
  for name, value in pairs( response_event ) do
    if suppress:find( name ) == nil then
      meta[ name ] = value
    end
  end

  local response_data = response_event.response or nil
  if type( response_data ) == 'string' then
    --json safe
    ok, res = json2table( response_data )
  elseif type( response_data ) == 'table' then
    --passing through
    ok = true
    res = response_data
  end

  debug:process( res, meta, request_data, force_debug, response.call_count )

  return ok, res, meta
end

return response

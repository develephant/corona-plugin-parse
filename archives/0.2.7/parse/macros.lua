--== Default Macro Definitions ==--
local config  = require('plugin.parse.config')
local tools  = require('plugin.parse.tools')
local api  = require('plugin.parse.endpoints')
local req = require('plugin.parse.request')
--============================================================================--
local macro = {}

--============================================================================--
--== Linking
--============================================================================--
function macro.linkObjectToUser( linkCol, className, objectId, userId )
  return req( api.User.update, userId )
  :data({
    [linkCol] = { ["__type"] = config.Type.Pointer, className = className, objectId = objectId }
  })
end

function macro.linkFileToUser( fileCol, fileUri, userId )
  return req( api.User.update, userId )
  :data({
    [fileCol] = { ["__type"] = config.Type.File, name = fileUri }
  })
end

function macro.linkInstallationToUser( installCol, installId, userId )
  return req( api.Installation.update, installId )
  :data({
    [installCol] = { ["__type"] = config.Type.Pointer, className = config.Type.User, objectId = userId }
  })
end

function macro.linkFileToObject( fileCol, fileUri, className, objectId )
  return req( api.Object.update, className, objectId )
  :data({
    [fileCol] = { ["__type"] = config.Type.File, name = fileUri }
  })
end

function macro.linkUserToObject( userCol, className, objectId, userId )
  return req( api.Object.update, className, objectId )
  :data({
    [userCol] = { ["__type"] = config.Type.Pointer, className = config.Type.User, objectId = userId }
  })
end

function macro.linkObjectToUser( objectCol, className, objectId, userId )
  return req( api.User.update, userId )
  :data({
    [objectCol] = { ["__type"] = config.Type.Pointer, className = className, objectId = objectId }
  })
end

function macro.linkObjects( objectCol, className, objectId, toClassName, toObjectId )
  return req( api.Object.update, className, objectId )
  :data({
    [objectCol] = { ["__type"] = config.Type.Pointer, className = toClassName, objectId = toObjectId }
  })
end

--============================================================================--
--== Relationships (many-many)
--============================================================================--
function macro.addRelations( className, objectId, relationColName, relateObjectsTbl )
  local relateObjectsTbl = relateObjectsTbl or {}
  local relateObjects = {}
  local relObj
  for _, obj in ipairs( relateObjectsTbl ) do
    relObj =
    {
      ["__type"] = config.type.Pointer,
      className = obj.className,
      objectId = obj.objectId
    }
    table.insert( relateObjects, relObj )
  end

  return req( api.Object.update, className, objectId )
  :data({
    [relationColName] =
    {
      ["__op"] = config.Op.AddRelation,
      objects = relateObjects
    }
  })
end

function macro.removeRelations( className, objectId, relationColName, relateObjectsTbl )
  local relateObjectsTbl = relateObjectsTbl or {}
  local relateObjects = {}
  local relObj
  for _, obj in ipairs( relateObjectsTbl ) do
    relObj =
    {
      ["__type"] = config.type.Pointer,
      className = obj.className,
      objectId = obj.objectId
    }
    table.insert( relateObjects, relObj )
  end

  return req( api.Object.update, className, objectId )
  :data({
    [relationColName] =
    {
      ["__op"] = config.Op.RemoveRelation,
      objects = relateObjects
    }
  })
end

function macro.fetchRelations( className, relateColName, relateClassName, relateObjectId )
  return req( api.Object.query, className )
  :where({
    ["$relatedTo"] =
    {
      object =
      {
        ["__type"] = config.Type.Pointer,
        className = relateClassName,
        objectId = relateObjectId
      },
      key = relateColName
    }
  })
end

--============================================================================--
--== Inc/Dec Macros
--============================================================================--

function macro.updateCounter( counterCol, amount, className, objectId )
  return req( api.Object.update, className, objectId )
  :data({
    [counterCol] = { ["__op"] = config.Op.Increment, amount = amount }
  })
end

--============================================================================--
--== Special Column Objects
--============================================================================--

function macro.Pointer( className, objectId )
  return
  {
    ["__type"] = config.Type.Pointer,
    className = className,
    objectId = objectId
  }
end

function macro.GeoPoint( latitude, longitude )
  return { ["__type"] = config.Type.GeoPoint, latitude = latitude, longitude = longitude }
end

function macro.Binary( b64_data )
  return { ["__type"] = config.Type.Binary, base64 = b64_data }
end

function macro.Date( iso_date )
  return { ["__type"] = config.Type.Date, iso = iso_date }
end

--============================================================================--
--== Special Column Types
--============================================================================--
-- function macro.addGeoPointCol( geoPointCol, lat, long, className, objectId )
--   return req( api.Object.update, className, objectId )
--   :data({
--     [geoPointCol] = { ["__type"] = config.Type.GeoPoint, latitude = lat, longitude = long }
--   })
-- end
--
-- function macro.addBinaryCol( binaryCol, b64Data)
--   return req( api.Object.update, className, objectId )
--   :data({
--     [binaryCol] = { ["__type"] = config.Type.Bytes, base64 = b64Data }
--   })
-- end
--
-- function macro.addRelationCol( relationCol, relatedClassName, className, objectId )
--   return req( api.Object.update, className, objectId )
--   :data({
--     [relationCol] = { ["__type"] = config.Type.Relation, className = relatedClassName }
--   })
-- end
--
-- function macro.addPointerCol( pointerCol, pointerClass, pointerId, className, objectId )
--   return req( api.Object.update, className, objectId )
--   :data({
--     [pointerCol] = { ["__type"] = config.Type.Pointer, className = pointerClass, objectId = pointerId }
--   })
-- end
--
-- function macro.addDateCol( dateCol, date, className, objectId )
--   return req( api.Object.update, className, objectId )
--   :data({
--     [dateCol] = { ["__type"] = config.Type.Date, iso = date }
--   })
-- end

--============================================================================--
--== Auth Linking (twitter, fb, etc.)
--============================================================================--

-- Twitter
-- "id": "12345678",
-- "screen_name": "ParseIt",
-- "consumer_key": "SaMpLeId3X7eLjjLgWEw",
-- "consumer_secret": "SaMpLew55QbMR0vTdtOACfPXa5UdO2THX1JrxZ9s3c",
-- "auth_token": "12345678-SaMpLeTuo3m2avZxh5cjJmIrAfx4ZYyamdofM7IjU",
-- "auth_token_secret": "SaMpLeEb13SpRzQ4DAIzutEkCE2LBIm2ZQDsP3WUU"
function macro.addTwitterAuth( auth_tbl, userId )
  return req( api.User.update, userId )
  :data({
    authData =
    {
      twitter = auth_tbl
    }
  })
end

-- Facebook
-- "id": "123456789",
-- "access_token": "SaMpLeAAibS7Q55...",
-- "expiration_date": "2012-02-28T23:49:36.353Z"
function macro.addFacebookAuth( auth_tbl, userId )
  return req( api.User.update, userId )
  :data({
    authData =
    {
      facebook = auth_tbl
    }
  })
end

-- Anon
-- A uuid
function macro.addAnonAuth( uuid, userId )
  return req( api.User.update, userId )
  :data({
    authData =
    {
      anonymous = { id = uuid }
    }
  })
end

-- Facebook login
function macro.facebookAuthLogin( authData )
  return req( api.User.create )
  :data({
	   ["authData"] = authData
  })
end

-- Twitter login
function macro.twitterAuthLogin( authData )
  return req( api.User.create )
  :data({
    ["authData"] = authData
  })
end

-- Anon
function macro.anonAuthLogin( authData )
  return req( api.User.create )
  :data({
    ["authData"] = authData
  })
end

--============================================================================--
--== Specialized Queries
--============================================================================--

--Key searchs
function macro.findWhereGreater( className, key, value )
  return req( api.Object.query, className )
  :where({ [key] = { ["$gt"] = value } })
end

function macro.findWhereLess( className, key, value )
  return req( api.Object.query, className )
  :where({ [key] = { ["$lt"] = value } })
end

function macro.findWhereEqual( className, key, value )
  return req( api.Object.query, className )
  :where({ [key] = { ["$eq"] = value } })
end

function macro.findWhereEqualOrLess( className, key, value )
  return req( api.Object.query, className )
  :where({ [key] = { ["$lte"] = value } })
end

function macro.findWhereEqualOrGreater( className, key, value )
  return req( api.Object.query, className )
  :where({ [key] = { ["$gte"] = value } })
end

function macro.findWhereNotEqual( className, key, value )
  return req( api.Object.query, className )
  :where({ [key] = { ["$ne"] = value } })
end

function macro.findWhereEmpty( className, key )

end

function macro.findWhereNotEmpty( className, key )

end

function macro.findWhereTrue( className, key )
  return req( api.Object.query, className )
  :where({ [key] = true })
end

function macro.findWhereFalse( className, key )
  return req( api.Object.query, className )
  :where({ [key] = false })
end

-- Arrays

-- function macro.findWhereInArray()
--
-- end
--
-- function macro.findWhereNotInArray()
--
-- end
--
-- function macro.findWhereAllInArray()
--
-- end
--
-- function macro.addToArray()
--
-- end
--
-- function macro.addUniqueToArray()
--
-- end
--
-- function macro.removeFromArray()
--
-- end
--
-- -- Count
--
-- function macro.objectCount( className, query )
--   local query = query or {}
--
-- end

-- Combined data

-- function macro.fetchObjectWith( className, objectId, linkColNamesTblArr )
--
--
--   return req( api.Object.get, className, objectId )
--   :options( include = )
-- end

-- Users

function macro.findUserByEmail( email )
  return req( api.User.query )
  :where({ email = email })
end

function macro.findUserByUsername( username )
  return req( api.User.query )
  :where({ username = username })
end

function macro.findUserById( userId )
  return req( api.User.query )
  :where({ objectId = userId })
end

--GeoSearch

-- max radius in miles is 100
function macro.geoSearchWithinRadius( className, latitude, longitude )
  return req( parse.Object.query, className )
  :where({location =
    {
      ["$nearSphere"] =
      {
        ["__type"] = "GeoPoint",
        latitude = latitude,
        longitude = longitude
      }
    }
  })
end

function macro.geoSearchWithinMiles( className, latitude, longitude, withinMiles )
  return req( parse.Object.query, className )
  :where({location =
    {
      ["$nearSphere"] =
      {
        ["__type"] = "GeoPoint",
        latitude = latitude,
        longitude = longitude
      },
      ["$maxDistanceInMiles"] = withinMiles
    }
  })
end

function macro.geoSearchWithinKms( className, latitude, longitude, withinKms )
  return req( parse.Object.query, className )
  :where({location =
    {
      ["$nearSphere"] =
      {
        ["__type"] = "GeoPoint",
        latitude = latitude,
        longitude = longitude
      },
      ["$maxDistanceInKilometers"] = withinKms
    }
  })
end

function macro.geoSearchWithinRadians( className, latitude, longitude, withinRadians )
  return req( parse.Object.query, className )
  :where({location =
    {
      ["$nearSphere"] =
      {
        ["__type"] = "GeoPoint",
        latitude = latitude,
        longitude = longitude
      },
      ["$maxDistanceInRadians"] = withinRadians
    }
  })
end

function macro.geoSearchWithinBoundary( className, swLatitude, swLongitude, neLatitude, neLongitude)
  return req( parse.Object.query, className )
  :where({location =
    {
      ["$within"] =
      {
        ["$box"] = {
          {
            ["__type"] = "GeoPoint",
            latitude = seLatitude,
            longitude = seLongitude
          },
          {
            ["__type"] = "GeoPoint",
            latitude = nwLatitude,
            longitude = nwLongitude
          }
        }
      }
    }
  })
end

--============================================================================--
--== Custom User Macros
--============================================================================--
function macro.add( macroName, macroReq )
  if macro[macroName] then
    print("Name in use:", macroName)
    return nil
  else
    macro[macroName] = macroReq
  end
end

function macro.remove( macroName )
  if macro[macroName] then
    macro[macroName] = nil
  end
end


return macro

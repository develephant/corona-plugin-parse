--======================================================================--
--== Corona Parse Plugin Tests
local parse = require "plugin.parse"
--======================================================================--

--== Parse Plugin Tests
-- This is a set of random tests. You will need to supply your own
-- credentials to use them. The full API with more code demonstration
-- is available at http://parse.develephant.com
-- Get a Parse account at https://parse.com

--==============================================================================
--== Preflight
--==============================================================================

--== Set Parse Credentials
parse.config:cloudAddress("http://159.203.12.49:32786/1")
parse.config:applicationId("23e27183-67ea-4d33-9b63-005aa801f5b4")
parse.config:installationId("83b251da-ca7d-4aca-965e-062811b687c3")

--parse.config:masterKey("d994cca9-b24e-4d66-90ca-0e7a7e48a493")

--== Set Debugging Output Modes
parse.config:debugEnabled( true )
parse.config:debugVerbose( true )

-- parse.request( parse.Object.get, "Fun", "iIsYRanLzv" )
-- :response(function(ok, res) {
--
-- })

-- local b = parse.batch.new()
-- b.create("Fun", {pet = "cat", age = 10})
-- b.create("Fun", {pet = "dog", age = 33})
-- b.create("Fun", {pet = "turtle", age = 123})
-- parse.request( parse.Object.batch )
-- :data( b.getBatch() )
-- :response()

-- parse.request( parse.Object.create, "Fun" )
-- :set("smokey", "cat")
-- :set("score", 12)
-- :response()
--
-- parse.request( parse.Object.create, "Fun" )
-- :set("smokey", "mouse")
-- :set("score", 52)
-- :response()

--zXZuhR9FSo

-- parse.request( parse.Object.get, "Fun", "EfsWNSSaFd" )
-- :response()

-- parse.request( parse.Cloud.call, "hello" )
-- :data({name="Timbo"})
-- :response()

-- parse.request( parse.User.query )
-- :where({})
-- :response()

-- parse.request( parse.Analytics.AppOpened )
-- :data({})
-- :response()

-- parse.request( parse.Analytics.event, "Clicked" )
-- :data({dimensions={source = "craigslist"}})
-- :response()

-- parse.request( parse.User.create )
-- :data({
--   username = "smokey",
--   password = "luckyjim"
-- })
-- :response()

--gx1BKoZPhp
--objectId: trBpPXGnzj
--sessionToken: r:3f632ce56ec8219a49e8dacab3b48b1a


-- parse.request( parse.User.update, "trBpPXGnzj" )
-- :data({color="purple"})
-- :response()

-- parse.request( parse.User.get, "trBpPXGnzj" )
-- :response()

-- parse.request( parse.Session.query )
-- :where({})
-- :response()

parse.request( parse.User.login )
:options({username="smokey",password="luckyjim"})
:response()

-- parse.request( parse.Session.query )
-- :where({})
-- :response()

-- local device_id = system.getInfo('deviceID')
--
-- function generateInstallationId( input )
--   local crypto = require( "crypto" )
--   local hash = crypto.digest( crypto.md5, input )
--   local function h( s, e )
--     return string.sub(hash, s, e)
--   end
--   local id = h(1,8)..'-'..h(9,12)..'-'..h(13,16)..'-'..h(17,20)..'-'..h(21,nil)
--   return id
-- end
--
-- local install_id = generateInstallationId( device_id )
--
-- parse.config:installationId( install_id )
--
-- local userId = ''
-- local userTmp = 'b'
--
-- print( parse.config.version )
-- --==============================================================================
-- --== userPref
-- --==============================================================================
--
-- local function onUpdateUserPref( ok, res )
--   -- if not ok then
--   --   --Error
--   --   print( "Error", res.error )
--   -- else
--   --   parse.request( parse.User.logout )
--   --   :response(function( ok )
--   --     print( parse.config:sessionToken() )
--   --   end)
--   -- end
--   print('done')
-- end
--
-- local function addUserPref( ok, res )
--   if not ok then
--     --Error
--     print( "Error", res.error )
--   else
--     parse.request( parse.User.update, userId )
--     :set("fav_color", "yellow")
--     :response(onUpdateUserPref)
--   end
-- end
-- --==============================================================================
-- --== addObject
-- --==============================================================================
--
-- local function addObject( color )
--   parse.request( parse.Object.create, "Prefs" )
--   :set("color", color)
--   :response(addUserPref)
-- end
--
-- --==============================================================================
-- --== createUser
-- --==============================================================================
--
--
-- local function onUserCreate( ok, res )
--   if not ok then
--     --Error
--     print( "Could not create user.", res.error )
--   else
--     userId = res.objectId
--     sessionToken = res.sessionToken
--
--     addObject("blue")
--   end
-- end
--
-- local function createUser()
--   --== Create a New User
--   local username = userTmp..'User'
--   local email = string.format("noreply%s@parse.com", userTmp)
--
--   parse.request( parse.User.create )
--   :data({username=username,email=email,password="1234"})
--   :response(onUserCreate)
-- end
--
-- createUser()


-- function newObject()
--
--   -- parse.request( parse.Object.update, "Turtles", "eMfmtE0AjY" )
--   -- :data({color="Blue",age=12})
--   -- :response()
--
--   -- parse.request( parse.Object.create, "Turtles")
--   -- :data({color="Orange",age=20})
--   -- :response()
--
--   parse.request( parse.User.update, "WQNHPDxE81" )
--   :data({color="Blue",age=12})
--   :response()
--
--   -- timer.performWithDelay( 10000, function()
--   --
--   --   parse.request( parse.User.logout )
--   --   :response()
--   --
--   -- end )
--
--   -- parse.request( parse.Session.me )
--   -- :response()
--
--
--
-- end
--newObject()

-- function login()
--
--   parse.request( parse.User.login )
--   :options( { username = "snookie", password = "1234" } )
--   :response( newObject )
--
-- end

-- function loginSession()
--
-- --r:UjgsAjEF1XY1SPVvErllwWSBl
--
--   parse.config:sessionToken("r:UjgsAjEF1XY1SPVvErllwWSBl")
--
--   parse.request( parse.Session.me )
--   :response(newObject)
--
--
--
-- end
--
-- loginSession()
-- login()

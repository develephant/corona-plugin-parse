--======================================================================--
--== Corona Parse Plugin Tests
local parse = require "plugin.parse"
--======================================================================--

--== Parse Plugin Tests
-- This is a set of random tests. You will need to supply your own
-- credentials to use them. The full API with more code demonstration
-- is available at http://parse.develephant.com
-- Get a Parse account at https://parse.com

--==============================================================================
--== Preflight
--==============================================================================

--== Set Parse Credentials
parse.config:applicationId("Kn3XJZyLpysgUg5mxErnL4bVGIGYxofV6VwFi1Qa")
parse.config:restApiKey("bWjq257j6BYyKGiyc62HVMvYps5EclzbOWbcuJuf")

--== Set Debugging Output Modes
parse.config:debugEnabled( true )
parse.config:debugVerbose( true )

parse.config:installationId("2d3777a5-f5fc-4caf-80be-73c766235afb")

--== Create a New User
-- parse.request( parse.User.create )
-- :data({username="snookie",email="noreply@develephant.com",password="1234"})
-- :response()

function newObject()

  -- parse.request( parse.Object.update, "Turtles", "eMfmtE0AjY" )
  -- :data({color="Blue",age=12})
  -- :response()

  -- parse.request( parse.Object.create, "Turtles")
  -- :data({color="Orange",age=20})
  -- :response()

  parse.request( parse.User.update, "WQNHPDxE81" )
  :data({color="Blue",age=12})
  :response()

  -- timer.performWithDelay( 10000, function()
  --
  --   parse.request( parse.User.logout )
  --   :response()
  --
  -- end )

  -- parse.request( parse.Session.me )
  -- :response()



end
--newObject()

-- function login()
--
--   parse.request( parse.User.login )
--   :options( { username = "snookie", password = "1234" } )
--   :response( newObject )
--
-- end

function loginSession()

--r:UjgsAjEF1XY1SPVvErllwWSBl

  parse.config:sessionToken("r:UjgsAjEF1XY1SPVvErllwWSBl")

  parse.request( parse.Session.me )
  :response(newObject)



end

loginSession()
-- login()

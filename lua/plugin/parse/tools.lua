---parse.tools
-- @author Chris Byerley
local json  = require('json')
local mime  = require('mime')
local url   = require('socket.url')

local tools = {}

function tools.generateInstallationId( input )
  assert(input, "An input string is a required parameter.")

  local crypto = require( "crypto" )
  local hash = crypto.digest( crypto.md5, input )
  local function h( s, e )
    return string.sub(hash, s, e)
  end
  local id = h(1,8)..'-'..h(9,12)..'-'..h(13,16)..'-'..h(17,20)..'-'..h(21,nil)
  return id
end

function tools.escape( value )
  local v = (url.escape(value))
  return v
end

function tools.unescape( value )
  local v = (url.unescape(value))
  return v
end

function tools.b64( value )
  return (mime.b64(value))
end

function tools.timestampToISODate( unixTimestamp )
  --2013-12-03T19:01:25Z"
  unixTimestamp = unixTimestamp or os.time()
  return os.date( "!%Y-%m-%dT%H:%M:%SZ", unixTimestamp )
end

function tools.timestampToDate( unixTimestamp )
  --2013-12-03 19:01:25 "
  unixTimestamp = unixTimestamp or os.time()
  return os.date( "!%m-%d %H:%M:%S", unixTimestamp )
end

--== "Safe" JSON handlers
function tools.json2table( json_str )
  local ok, res
  if type( json_str ) ~= 'table' then
    ok, res = pcall( json.decode, json_str )
  end

  return ok, res
end

function tools.table2json( data_tbl )
  local ok, res
  if type( data_tbl ) == 'table' then
    ok, res = pcall( json.encode, data_tbl )
  end

  return ok, res
end

function tools.trace( t, indent )
-- print contents of a table, with keys sorted. second parameter is optional, used for indenting subtables
  local names = {}
  if not indent then indent = "" end
  for n,g in pairs(t) do
      table.insert(names,n)
  end
  table.sort(names)
  for i,n in pairs(names) do
      local v = t[n]
      if type(v) == "table" then
          if(v==t) then -- prevent endless loop if table contains reference to itself
              print(indent..tostring(n)..": <-")
          else
              print(indent..tostring(n)..":")
              tools.trace(v,indent.."   ")
          end
      else
          if type(v) == "function" then
              print(indent..tostring(n).."()")
          else
              print(indent..tostring(n)..": "..tostring(v))
          end
      end
  end
end

return tools

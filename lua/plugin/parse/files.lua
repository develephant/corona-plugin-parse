---parse files module
-- @author Chris Byerley
local config    = require('plugin.parse.config')
local endpoints = require('plugin.parse.endpoints')
local mimetypes = require('plugin.parse.mimetypes')
local response  = require('plugin.parse.response')

local files = {}

--== File Uploads
function files.upload( filename, baseDir )
  local filename = filename or nil
  local baseDir = baseDir or system.DocumentsDirectory
  
  local up =
  {
    callback = nil,
    progress_cb = nil,
    progress_evt = false
  }
  
  function up:progress( progress_cb )
    local progress_cb = progress_cb or nil
    
    if progress_cb then
      self.progress_evt = true
      self.progress_cb = progress_cb
    end
    return self
  end
  
  function up:response( callback )
    
    self.callback = callback
    
    local request_data = nil
    
    --== Filename?
    if filename == nil then
      self.callback( false, "A filename is required", nil )
      return
    end
    
    self.filename = filename
    
    --== Content type
    local content_type = mimetypes.guess( self.filename )
    
    if content_type == nil then
      self.callback( false, "This filetype is unsupported.", nil )
      return
    end
    
    local function _listener( event )
      if event.isError then
        self.callback( false, 'network error', nil )
      elseif( event.phase == 'began' ) then
      elseif( event.phase == 'progress' ) then
        if self.progress_evt and self.progress_cb then
          self.progress_cb( true, event.bytesTransferred, event.bytesEstimated )
        end
      elseif( event.phase == 'ended' ) then
        local ok, res, res_meta = response.parse( event, request_data )
        if ( self.callback ) then
          self.callback( ok, res, res_meta )
        end
        return
      end
    end
    
    local _params =
    {
      headers =
      {
        ["X-Parse-Application-Id"] = config:applicationId(),
        ["X-Parse-REST-API-Key"] = config:restApiKey(),
        ["Content-Type"] = content_type
      },
      progress = self.progress_evt
    }

    local url_tpl = "/files/%s"
    local verb = "POST"
    
    local req_url = string.format( url_tpl, filename )
    req_url = "https://api.parse.com/1"..req_url
    
    request_data =
    {
      headers = _params.headers,
      file_name = filename,
      file_type = content_type
    }

    network.upload( req_url, verb, _listener, _params, filename, baseDir )
  end
  
  return up

end

--== File Download
function files.download( file_url, save_as_name, baseDir )

  local down =
  {
    file_url = file_url,
    save_as_name = save_as_name,
    baseDir = baseDir or system.DocumentsDirectory,
    progress_evt = false,
    progress_cb = nil,
    callback = nil
  }
  
  function down:progress( progress_cb )
    if progress_cb then
      self.progress_evt = true
      self.progress_cb = progress_cb
    end
    return self
  end
  
  function down:response( callback )
        
    if callback then
      self.callback = callback
    end
    
    local request_data = nil
    
    local function _listener( event )
      if event.isError then
        self.callback( false, { error = "network error" }, request_data )
        return
      elseif( event.phase == 'began' ) then
      elseif( event.phase == 'progress' ) then
        if self.progress_evt and self.progress_cb then
          self.progress_cb( true, event.bytesTransferred, event.bytesEstimated )
        end
      elseif( event.phase == 'ended' ) then
        local ok, res, res_meta = response.parse( event, request_data )
        self.callback( ok, res, res_meta )
        return
      end
    end
    
    local _params =
    {
      progress = self.progress_evt
    }
    
    --== ?
    request_data =
    {
      headers = _params.headers,
      file_url = self.file_url,
      file_destination = self.save_as_name
    }

    network.download( self.file_url, "GET", _listener, _params, self.save_as_name, self.baseDir )
  end
  
  return down

end

return files

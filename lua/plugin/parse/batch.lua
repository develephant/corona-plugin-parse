---parse.batch
-- @author Chris Byerley
local api = require( "plugin.parse.endpoints" )
local tools = require( "plugin.parse.tools" )
local m = {}

function m.new()
  local b =
  {
    requests = {},
    max_reqs = 50
  }
  
  b.getPath = function( ep, ... )
    local path = string.format( '/1'..ep.tpl, ... )
    return path
  end
  
  b.getCount = function()
    return #b.requests
  end
  
  b.addEntry = function( entry )
    if b.getCount() <= b.max_reqs then
      table.insert( b.requests, entry )
    end
  end
  
  b.create = function( className, dataTbl )
    b.addEntry({
      method  = "POST",
      path    = b.getPath( api.Object.create, className ),
      body    = dataTbl
    })
  end
  
  b.update = function( className, objectId, dataTbl )
    b.addEntry({
      method  = "PUT",
      path    = b.getPath( api.Object.update, className, objectId ),
      body    = dataTbl
    })
  end
  
  b.delete = function( className, objectId )
    b.addEntry({
      method  = "DELETE",
      path    = b.getPath( api.Object.delete, className, objectId )
    })
  end
  
  b.getBatch = function()
    return { requests = b.requests }
  end
  
  return b
end

return m

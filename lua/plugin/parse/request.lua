---parse.request
-- @author Chris Byerley
-- @contributors @shambo (Appduction Studios)
local json = require('json')

local config = require('plugin.parse.config')
local tools = require('plugin.parse.tools')
local response = require('plugin.parse.response')

local request = function( action, ... )
  local meta

  local api_base = config:cloudAddress()

  local req =
  {
    api_base = api_base,
    action = action,
    params = { ... },
    callback = nil,
    data_tbl = {},
    add_headers = {}, --additional headers
    query_str = nil, -- `where` queries,
    query_opts = nil, -- result modifiers
  }

  function req:header( name, value )
    if name and value then
      self.add_headers[ name ] = value
    end
    return self
  end

  --QUERY, can take raw JSON
  function req:where( query_tblOrStr )
    if query_tblOrStr then
      local query_str = query_tblOrStr

      if type( query_str ) == 'table' then
        query_str = json.encode( query_str )
      end
      self.query_str = "where=".. tools.escape( query_str )
    end
    return self
  end

  --OPTIONS, can take raw text string
  -- "count=2&limit=2&order=color"
  function req:options( opts_tblOrStr )
    if opts_tblOrStr then
      self.query_opts = opts_tblOrStr
    end
    return self
  end

  --OPTIONS direct
  -- function req:count()
  --   if type( self.query_opts ) == 'table' then
  --     self.query_opts.count = true
  --   end
  --   return self
  -- end

  function req:order( orderStr )
    if type( self.query_opts ) == 'table' then
      self.query_opts.order = orderStr
    end
    return self
  end

  function req:limit( amount )
    if type( self.query_opts ) == 'table' then
      self.query_opts.limit = amount
    end
    return self
  end

  function req:skip( amount )
    if type( self.query_opts ) == 'table' then
      self.query_opts.skip = amount
    end
    return self
  end

  function req:include( includeTblArr )
    if type( self.query_opts ) == 'table' then
      self.query_opts.include = table.concat( includeTblArr, "." )
    end
    return self
  end

  function req:keys( keysTblArr )
    if type( self.query_opts ) == 'table' then
      self.query_opts.keys = table.concat( keysTblArr, "," )
    end
    return self
  end

  --POST/PUT, can take raw JSON
  function req:data( data_tblOrStr )
    if data_tblOrStr then
      self.data_tbl = data_tblOrStr
    end
    return self
  end

  function req:set( key, value )

    if type( self.data_tbl ) == 'table' then
      self.data_tbl[ key ] = value
    end

    return self
  end

  function req:response( callback )
    self.callback = callback or nil

    local request_data

    --check for correct arguments
    if #self.params ~= #self.action.keys then
      local keys = table.concat( self.action.keys, ", " )
      local msg = "parse.request: A key or keys are missing. Expected: "..keys

      if self.callback then
        self.callback( false, { error = msg }, nil )
      else
        print( "ERROR >> " .. msg )
      end

      return
    end

    local function _checkSession( res )
      if res and res.sessionToken then
        config:sessionToken( res.sessionToken )
      end
    end

    local function _callback( ok, res, meta )
      if self.callback then
        self.callback( ok, res, meta )
      end
    end

    local url_tpl = self.action.tpl

    local req_url = string.format( url_tpl, unpack( self.params ) )
    req_url = self.api_base..req_url

    local req_method = self.action.verb
    local req_params = nil

    local req_contentType = 'application/json'

    local req_applicationId = config:applicationId()
    local req_restApiKey = config:restApiKey()

    --==make request
    local function req_listener( event )

      if event.phase == 'ended' then

        --==============================================================================
        --== SPECIAL CASES
        if self.action.tpl == "/logout" then
          config:clearSession()
        end
        --==============================================================================

        if event.isError then
          _callback( false, { error = "network error" }, meta )
          return
        else
          --== Lets work the response
          local ok, res, res_meta = response.parse( event, request_data )
          if ok then
            _checkSession( res )
          end

          _callback( ok, res, res_meta )
          return
        end
      end
    end

    --== Set up request parameters
    request_data =
    {
      headers =
      {
        ["X-Parse-Application-Id"] = req_applicationId,
        ["X-Parse-REST-API-Key"] = req_restApiKey,
        ["Content-Type"] = req_contentType,
        ["Accept"] = 'application/json'
      }
    }

    --== Select the appropiate API key
    if config:masterKey() then
      self.add_headers["X-Parse-Master-Key"] = config:masterKey()
    else

    end

    --== Do we have a session going?
    if config:sessionToken() then
      self.add_headers['X-Parse-Session-Token'] = config:sessionToken()
    end

    --== Installation ID?
    if config:installationId() then
      self.add_headers['X-Parse-Installation-Id'] = config:installationId()
    end

    --== Additional headers
    if self.add_headers then
      for name, value in pairs( self.add_headers ) do
        request_data.headers[ name ] = value
      end
    end

    --== Check for query data
    local qtbl_buffer = {}

    if self.query_str ~= nil then
      table.insert( qtbl_buffer, self.query_str )
    end

    --== Check for query options
    if self.query_opts ~= nil then
      local t = type( self.query_opts )
      if t == 'table' then
        for name, value in pairs( self.query_opts ) do
          table.insert( qtbl_buffer, name.."="..tools.escape( tostring(value) ) )
        end
      elseif t == 'string' then
        table.insert( qtbl_buffer, tools.escape( self.query_opts ) )
      end
    end

    local function check_buff(qtbl_buffer)
      if #qtbl_buffer > 0 then
        return '?'
      end
      return ''
    end
    req_url = req_url .. check_buff(qtbl_buffer) .. table.concat( qtbl_buffer, "&" )

    --== Check for body data
    if self.data_tbl then
      local data_json = nil

      if type( self.data_tbl ) == 'table' then
        --== Encode
        data_json = json.encode( self.data_tbl )
      elseif type( self.data_tbl ) == 'string' then
        data_json = self.data_tbl
      end

      if data_json ~= nil then
        request_data.body = data_json
        request_data.headers["Content-Length"] = #data_json
      end
    end

    network.request( req_url, req_method, req_listener, request_data )
  end

  return req

end

return request

--needed?
-- function req:get( key )
--   if self.data_tbl then
--     if self.data_tbl[ key ] then
--       return self.data_tbl[ key ]
--     end
--   end
--   return nil
-- end

--== Account - USE MANUAL HEADERS
-- if config:accountEmail() then
--   if config:accountPasswd() then
--     self.add_headers['X-Parse-Email'] = config:accountEmail()
--     self.add_headers['X-Parse-Password'] = config.accountPasswd()
--   end
-- end

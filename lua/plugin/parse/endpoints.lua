---parse endpoint meta
-- @author Chris Byerley
local e =
{
  --objects
  Object =
  {
    create =
    {
      verb  = "POST",
      tpl   = "/classes/%s",
      keys  = { "class" }
    },
    get =
    {
      verb  = "GET",
      tpl   = "/classes/%s/%s",
      keys  = { "class", "objectId" }
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/classes/%s/%s",
      keys  = { "class", "objectId" }
    },
    query =
    {
      verb  = "GET",
      tpl   = "/classes/%s",
      keys  = { "class" }
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/classes/%s/%s",
      keys  = { "class", "objectId" }
    },
    batch =
    {
      verb  = "POST",
      tpl   = "/batch",
      keys  = {}
    }
  },
  --users
  User =
  {
    login =
    {
      verb  = "GET",
      tpl   = "/login",
      keys  = {}
    },
    logout =
    {
      verb  = "POST",
      tpl   = "/logout",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/users/%s",
      keys  = { "objectId" }
    },
    me =
    {
      verb  = "GET",
      tpl   = "/users/me",
      keys  = {}
    },
    create =
    {
      verb  = "POST",
      tpl   = '/users',
      keys  = {}
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/users/%s",
      keys  = { "objectId" }
    },
    query =
    {
      verb  = "GET",
      tpl   = "/users",
      keys  = {}
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/users/%s",
      keys  = { "objectId" }
    },
    requestPasswordReset =
    {
      verb  = "POST",
      tpl   = "/requestPasswordReset",
      keys  = {}
    },
    link =
    {
      verb  = "POST",
      tpl   = "/users/%s",
      keys  = { "objectId" }
    },
    unlink =
    {
      verb  = "PUT",
      tpl   = "/users/%s",
      keys  = { "objectId" }
    }
  },
  --sessions
  Session =
  {
    get =
    {
      verb = "GET",
      tpl   = "/sessions/%s",
      keys  = { "objectId" }
    },
    me =
    {
      verb  = "GET",
      tpl   = "/sessions/me",
      keys  = {}
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/sessions/%s",
      keys  = { "objectId" }
    },
    query =
    {
      verb  = "GET",
      tpl   = "/sessions",
      keys  = {}
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/sessions/%s",
      keys  = { "objectId" }
    },
    logout = --same as User.logout, kills session
    {
      verb  = "POST",
      tpl   = "/logout",
      keys  = {}
    }
  },
  --roles
  Role =
  {
    create =
    {
      verb  = "POST",
      tpl   = "/roles",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/roles/%s",
      keys  = { "objectId" }
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/roles/%s",
      keys  = { "objectId" }
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/roles/%s",
      keys  = { "objectId" }
    }
  },
  --files
  File =
  {
    --use parse.files module for upload/download
    link =
    {
      verb  = "POST",
      tpl   = "/classes/%s",
      keys  = { "class" }
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/files/%s",
      keys  = { "fileName" }
    }
  },
  --analytics
  Analytics =
  {
    AppOpened =
    {
      verb  = "POST",
      tpl   = "/events/AppOpened",
      keys  = {}
    },
    event =
    {
      verb  = "POST",
      tpl   = "/events/%s",
      keys  = { "eventName" }
    }
  },
  --push notification
  Push =
  {
    send =
    {
      verb  = "POST",
      tpl   = "/push",
      keys  = {}
    }
  },
  --installations
  Installation =
  {
    create =
    {
      verb  = "POST",
      tpl   = "/installations",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/installations/%s",
      keys  = { "objectId" }
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/installations/%s",
      keys  = { "objectId" }
    },
    query =
    {
      verb  = "GET",
      tpl   = "/installations",
      keys  = {}
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/installations/%s",
      keys  = { "objectId" }
    }
  },
  --config
  Config =
  {
    get =
    {
      verb  = "GET",
      tpl   = "/config",
      keys  = {}
    }
  },
  --cloud functions
  Cloud =
  {
    call =
    {
      verb  = "POST",
      tpl   = "/functions/%s",
      keys  = { "functionName" }
    },
    job =
    {
      verb  = "POST",
      tpl   = "/jobs/%s",
      keys  = { "jobName" }
    }
  },
  --schemas
  Schema =
  {
    getAll =
    {
      verb  = "GET",
      tpl   = "/schemas",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/schemas/%s",
      keys  = { "schemaName" }
    },
    create =
    {
      verb  = "POST",
      tpl   = "/schemas/%s",
      keys  = { "schemaName" }
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/schemas/%s",
      keys  = { "schemaName" }
    },
    delete =
    {
      verb  = "DELETE",
      tpl   = "/schemas/%s",
      keys  = { "schemaName" }
    }
  },
  --apps
  App =
  {
    getAll =
    {
      verb  = "GET",
      tpl   = "/apps",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/apps/%s",
      keys  = { "applicationId" }
    },
    create =
    {
      verb  = "POST",
      tpl   = "/apps",
      keys  = {}
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/apps/%s",
      keys  = { "applicationId" }
    }
  },
  --hooks
  Hook =
  {
    getAll =
    {
      verb  = "GET",
      tpl   = "/hooks/functions",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/hooks/functions/%s",
      keys  = { "functionName" }
    },
    create =
    {
      verb  = "POST",
      tpl   = "/hooks/functions",
      keys  = {}
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/hooks/functions/%s",
      keys  = { "functionName" }
    },
    delete =
    {
      verb  = "PUT",
      tpl   = "/hooks/functions/%s",
      keys  = { "functionName" }
    }
  },
  --triggers
  Trigger =
  {
    getAll =
    {
      verb  = "GET",
      tpl   = "/hooks/triggers",
      keys  = {}
    },
    get =
    {
      verb  = "GET",
      tpl   = "/hooks/triggers/%s/%s",
      keys  = { "class", "triggerName" }
    },
    create =
    {
      verb  = "POST",
      tpl   = "/hooks/triggers",
      keys  = {}
    },
    update =
    {
      verb  = "PUT",
      tpl   = "/hooks/triggers/%s/%s",
      keys  = { "class", "triggerName" }
    },
    delete =
    {
      verb  = "PUT",
      tpl   = "/hooks/triggers/%s/%s",
      keys  = { "class", "triggerName" }
    }
  }
  
}
return e

---Parse Plugin Module for Corona SDK.
-- @author C. Byerley (develephant)
-- @copyright 2015-18 C. Byerley
-- @module Parse
local Library = require "CoronaLibrary"
local lib = Library:new{ name='plugin.parse', publisherId='com.develephant' }

--== metatable with Parse endpoint data
local et = require('plugin.parse.endpoints')
setmetatable(lib, {__index=et})

--== Configutation / Options module
lib.config      = require('plugin.parse.config')

--== Main Request module
lib.request     = require('plugin.parse.request')

--== Macros
lib.macro       = require('plugin.parse.macros')

--== Object Batches
lib.batch       = require('plugin.parse.batch')

--== Misc Tools module
lib.tools       = require('plugin.parse.tools')

--== Debug output module
lib.debug       = require('plugin.parse.debug')

--== Upload / Download files module
lib.upload      = require('plugin.parse.files').upload
lib.download    = require('plugin.parse.files').download

--== "Safe" JSON methods (from tools)
lib.json2table  = require('plugin.parse.tools').json2table
lib.table2json  = require('plugin.parse.tools').table2json

--== Prints human readable table (from tools)
lib.trace       = require('plugin.parse.tools').trace

--== Parse type constants
lib.Type        = require('plugin.parse.config').Type
--== Parse op constants
lib.Op          = require('plugin.parse.config').Op

return lib

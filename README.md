# Corona SDK Parse Plugin

[![Documentation Status](http://readthedocs.org/projects/parse-corona-sdk-plugin/badge/?version=latest)](http://parse.develephant.com/?badge=latest)

View the full API documentation and usage instructions __[here](http://parse-corona-sdk-plugin.readthedocs.io/)__.
